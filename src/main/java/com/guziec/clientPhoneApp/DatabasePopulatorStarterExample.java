package com.guziec.clientPhoneApp;

import com.guziec.clientPhoneApp.model.Client;
import com.guziec.clientPhoneApp.model.PhoneNumber;
import com.guziec.clientPhoneApp.model.Profession;
import com.guziec.clientPhoneApp.repository.ClientRepository;
import com.guziec.clientPhoneApp.repository.PhoneNumberRepository;
import com.guziec.clientPhoneApp.repository.ProfessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabasePopulatorStarterExample implements CommandLineRunner {

    private ProfessionRepository professionRepository;
    private ClientRepository clientRepository;
    private PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public DatabasePopulatorStarterExample(ProfessionRepository professionRepository,
                                           ClientRepository clientRepository,
                                           PhoneNumberRepository phoneNumberRepository) {
        this.professionRepository = professionRepository;
        this.clientRepository = clientRepository;
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Profession cooker = createCooker();
        Profession doctor = createDoctor();
        Profession fireman = createFireman();
        Profession policeman = createPoliceman();

        createClient1(cooker);
        createClient2(doctor);
        createClient3(fireman);
        createClient4(policeman);
    }

    private Profession createDoctor() {
        Profession doctor = new Profession();
        doctor.setName("doctor");
        return professionRepository.save(doctor);
    }

    private Profession createCooker() {
        Profession cooker = new Profession();
        cooker.setName("cooker");
        return professionRepository.save(cooker);
    }

    private Profession createFireman() {
        Profession fireman = new Profession();
        fireman.setName("fireman");
        return professionRepository.save(fireman);
    }

    private Profession createPoliceman() {
        Profession policeman = new Profession();
        policeman.setName("policeman");
        return professionRepository.save(policeman);
    }

    private void createClient1(Profession profession){
        Client client = new Client();
        client.setFirstName("Wojtek");
        client.setAge(25);
        client.setProfession(profession);
        clientRepository.save(client);
    }

    private void createClient2(Profession profession){
        Client client = new Client();
        client.setFirstName("Mariusz");
        client.setAge(40);
        client.setProfession(profession);
        clientRepository.save(client);

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setNumber("502672189");
        phoneNumber.setClient(client);
        phoneNumberRepository.save(phoneNumber);
    }

    private void createClient3(Profession profession){
        Client client = new Client();
        client.setFirstName("Jan");
        client.setAge(55);
        client.setProfession(profession);
        clientRepository.save(client);

        PhoneNumber phoneNumber1 = new PhoneNumber();
        phoneNumber1.setNumber("503673589");
        phoneNumber1.setClient(client);
        phoneNumberRepository.save(phoneNumber1);

        PhoneNumber phoneNumber2 = new PhoneNumber();
        phoneNumber2.setNumber("751679129");
        phoneNumber2.setClient(client);
        phoneNumberRepository.save(phoneNumber2);
    }

    private void createClient4(Profession profession){
        Client client = new Client();
        client.setFirstName("Paweł");
        client.setAge(80);
        client.setProfession(profession);
        clientRepository.save(client);

        PhoneNumber phoneNumber1 = new PhoneNumber();
        phoneNumber1.setNumber("79173589");
        phoneNumber1.setClient(client);
        phoneNumberRepository.save(phoneNumber1);

        PhoneNumber phoneNumber2 = new PhoneNumber();
        phoneNumber2.setNumber("79641129");
        phoneNumber2.setClient(client);
        phoneNumberRepository.save(phoneNumber2);

        PhoneNumber phoneNumber3 = new PhoneNumber();
        phoneNumber3.setNumber("500679129");
        phoneNumber3.setClient(client);
        phoneNumberRepository.save(phoneNumber3);
    }
}
