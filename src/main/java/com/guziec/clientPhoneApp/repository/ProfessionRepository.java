package com.guziec.clientPhoneApp.repository;

import com.guziec.clientPhoneApp.model.Profession;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfessionRepository extends CrudRepository<Profession, Long> {
    Optional<Profession> findByName(String name);
}
