package com.guziec.clientPhoneApp.repository;

import com.guziec.clientPhoneApp.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
}
