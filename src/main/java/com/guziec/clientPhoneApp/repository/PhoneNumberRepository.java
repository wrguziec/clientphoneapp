package com.guziec.clientPhoneApp.repository;

import com.guziec.clientPhoneApp.model.PhoneNumber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PhoneNumberRepository extends CrudRepository<PhoneNumber, Long> {
    Optional<PhoneNumber> findByNumber(String number);
}
