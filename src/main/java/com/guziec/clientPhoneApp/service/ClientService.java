package com.guziec.clientPhoneApp.service;

import com.guziec.clientPhoneApp.exception.ProfessionNotFoundException;
import com.guziec.clientPhoneApp.model.Client;
import com.guziec.clientPhoneApp.model.Profession;
import com.guziec.clientPhoneApp.model.dto.ClientDTO;
import com.guziec.clientPhoneApp.model.dto.PhoneNumberDTO;
import com.guziec.clientPhoneApp.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private ClientRepository clientRepository;
    private ProfessionService professionService;

    @Autowired
    public ClientService(ClientRepository clientRepository, ProfessionService professionService) {
        this.clientRepository = clientRepository;
        this.professionService = professionService;
    }

    public List<ClientDTO> getAllClients() {
        List<Client> allClientsList = new ArrayList<>();
        Iterable<Client> allClientsIterable = clientRepository.findAll();
        allClientsIterable.forEach(allClientsList::add);
        return allClientsList.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    public boolean addClient(ClientDTO clientDTO) {
        Client client = mapToEntityWithoutPhoneNumbers(clientDTO);
        clientRepository.save(client);
        return true;
    }

    Optional<Client> findById(Long clientId) {
        return clientRepository.findById(clientId);
    }

    public Optional<ClientDTO> findDTOById(Long clientId) {
        return findById(clientId).map(this::mapToDTO);
    }

    private Client mapToEntityWithoutPhoneNumbers(ClientDTO clientDTO) {
        Client client = new Client();
        client.setId(clientDTO.getId());
        client.setFirstName(clientDTO.getFirstName());
        client.setAge(Integer.valueOf(clientDTO.getAge()));
        client.setPhoneNumbers(new ArrayList<>());
        Optional<Profession> profession = professionService.getByName(clientDTO.getProfessionName());
        if (!profession.isPresent()) {
            throw new ProfessionNotFoundException("name=" + clientDTO.getProfessionName());
        }
        client.setProfession(profession.get());
        return client;
    }

    private ClientDTO mapToDTO(Client client) {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setId(client.getId());
        clientDTO.setFirstName(client.getFirstName());
        clientDTO.setAge(String.valueOf(client.getAge()));
        String professionName = client.getProfession().getName();
        String businessProfessionName = professionService.getProfessionFromProperties(professionName);
        clientDTO.setProfessionName(businessProfessionName);
        List<PhoneNumberDTO> phoneNumberDTOList = client.getPhoneNumbers().stream()
                .map(PhoneNumberService::mapToDTO)
                .collect(Collectors.toList());
        clientDTO.setPhoneNumbersDTO(phoneNumberDTOList);
        return clientDTO;
    }

}
