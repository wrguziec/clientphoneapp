package com.guziec.clientPhoneApp.service;

import com.guziec.clientPhoneApp.model.Profession;
import com.guziec.clientPhoneApp.model.dto.ProfessionDTO;
import com.guziec.clientPhoneApp.repository.ProfessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProfessionService {

    private ProfessionRepository professionRepository;
    private MessageSource messageSource;

    @Autowired
    public ProfessionService(ProfessionRepository professionRepository, MessageSource messageSource) {
        this.professionRepository = professionRepository;
        this.messageSource = messageSource;
    }

    public List<ProfessionDTO> getAllProfessions() {
        List<Profession> allProfessionsList = new ArrayList<>();
        Iterable<Profession> allProfessionsIterable = professionRepository.findAll();
        allProfessionsIterable.forEach(allProfessionsList::add);
        return allProfessionsList.stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());
    }

    Optional<Profession> getByName(String name) {
        return professionRepository.findByName(name);
    }

    ProfessionDTO mapToDTO(Profession profession) {
        ProfessionDTO professionDTO = new ProfessionDTO();
        String businessProfessionName = getProfessionFromProperties(profession.getName());
        professionDTO.setBusinessName(businessProfessionName);
        professionDTO.setName(profession.getName());
        return professionDTO;
    }

    String getProfessionFromProperties(String professionName) {
        String professionProperty = "profession." + professionName;
        return messageSource.getMessage(professionProperty, null, new Locale("pl"));
    }
}
