package com.guziec.clientPhoneApp.service;

import com.guziec.clientPhoneApp.exception.ClientNotFoundException;
import com.guziec.clientPhoneApp.exception.PhoneNumberExists;
import com.guziec.clientPhoneApp.model.Client;
import com.guziec.clientPhoneApp.model.PhoneNumber;
import com.guziec.clientPhoneApp.model.dto.PhoneNumberDTO;
import com.guziec.clientPhoneApp.repository.PhoneNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class PhoneNumberService {

    private PhoneNumberRepository phoneNumberRepository;
    private ClientService clientService;

    @Autowired
    public PhoneNumberService(PhoneNumberRepository phoneNumberRepository, ClientService clientService) {
        this.phoneNumberRepository = phoneNumberRepository;
        this.clientService = clientService;
    }

    @Transactional
    public void addPhoneNumber(PhoneNumberDTO phoneNumberDTO) {
        boolean phoneNumberExists = phoneNumberExists(phoneNumberDTO);
        if(phoneNumberExists) {
            throw new PhoneNumberExists("NumberExists number=" + phoneNumberDTO.getNumber());
        }
        PhoneNumber phoneNumber = mapToEntity(phoneNumberDTO);
        phoneNumberRepository.save(phoneNumber);
    }

    private boolean phoneNumberExists(PhoneNumberDTO phoneNumberDTO) {
        return phoneNumberRepository.findByNumber(phoneNumberDTO.getNumber()).isPresent();
    }

    private PhoneNumber mapToEntity(PhoneNumberDTO phoneNumberDTO) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setId(phoneNumberDTO.getId());
        phoneNumber.setNumber(phoneNumberDTO.getNumber());
        Optional<Client> client = clientService.findById(phoneNumberDTO.getClientId());
        if(!client.isPresent()) {
            throw new ClientNotFoundException("phoneNumber=" + phoneNumberDTO.getNumber());
        }
        phoneNumber.setClient(client.get());
        return phoneNumber;
    }

    static PhoneNumberDTO mapToDTO(PhoneNumber phoneNumber) {
        PhoneNumberDTO phoneNumberDTO = new PhoneNumberDTO();
        phoneNumberDTO.setId(phoneNumber.getId());
        phoneNumberDTO.setNumber(phoneNumber.getNumber());
        phoneNumberDTO.setClientId(phoneNumber.getClient().getId());
        return phoneNumberDTO;
    }
}
