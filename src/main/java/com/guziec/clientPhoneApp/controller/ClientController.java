package com.guziec.clientPhoneApp.controller;

import com.guziec.clientPhoneApp.model.dto.ClientDTO;
import com.guziec.clientPhoneApp.model.dto.PhoneNumberDTO;
import com.guziec.clientPhoneApp.service.ClientService;
import com.guziec.clientPhoneApp.service.PhoneNumberService;
import com.guziec.clientPhoneApp.service.ProfessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/clients")
public class ClientController {

    private ClientService clientService;
    private PhoneNumberService phoneNumberService;
    private ProfessionService professionService;

    @Autowired
    public ClientController(ClientService clientService,
                            PhoneNumberService phoneNumberService,
                            ProfessionService professionService) {
        this.clientService = clientService;
        this.phoneNumberService = phoneNumberService;
        this.professionService = professionService;
    }

    @GetMapping
    public ModelAndView getAllClients() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("clientsList", clientService.getAllClients());
        modelAndView.setViewName("clientsList");
        return modelAndView;
    }

    @GetMapping("/refresh")
    public ModelAndView refreshClients() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("clientsList", clientService.getAllClients());
        modelAndView.setViewName("clientsList :: clientTable");
        return modelAndView;
    }

    @GetMapping("/add")
    public ModelAndView addClient() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("client", new ClientDTO());
        modelAndView.addObject("professionList", professionService.getAllProfessions());
        modelAndView.setViewName("addClient");
        return modelAndView;
    }

    @PostMapping("/add")
    public ModelAndView addClient(@Valid @ModelAttribute("client") ClientDTO clientDTO, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            modelAndView.addObject("client", clientDTO);
            modelAndView.addObject("professionList", professionService.getAllProfessions());
            modelAndView.setViewName("addClient");
            return modelAndView;
        }
        clientService.addClient(clientDTO);
        modelAndView.setViewName("redirect:/clients");
        return modelAndView;
    }

    @PostMapping("/addPhoneNumber")
    public String addPhoneNumber(@Valid @RequestBody PhoneNumberDTO phoneNumberDTO, Model model) {
        Long clientId = phoneNumberDTO.getClientId();
        phoneNumberService.addPhoneNumber(phoneNumberDTO);
        ClientDTO client = clientService.findDTOById(clientId).get();
        model.addAttribute("client", client);
        return "clientsList :: phoneList-client" + clientId;
    }
}
