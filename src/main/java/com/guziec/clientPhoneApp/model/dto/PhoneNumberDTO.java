package com.guziec.clientPhoneApp.model.dto;

import javax.validation.constraints.Pattern;

public class PhoneNumberDTO {

    private Long id;

    @Pattern(regexp = "^[1-9][0-9]{8}$")
    private String number;
    private Long clientId;

    public PhoneNumberDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
}
