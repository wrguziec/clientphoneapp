package com.guziec.clientPhoneApp.model.dto;

import javax.validation.constraints.Pattern;
import java.util.List;

public class ClientDTO {

    private Long id;

    @Pattern(regexp = "^[A-ZŻŹĆĄŚĘŁÓŃ]{1}[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ]*$",
    message = "Imię musi być jednym wyrazem pisanym z wielkiej litery bez cyfr i znaków specjalnych")
    private String firstName;

    @Pattern(regexp = "^(2[5-9]|[3-7][0-9]|80)$",
            message = "Wiek musi znajdować się w przedziale 25 do 80")
    private String age;
    private List<PhoneNumberDTO> phoneNumbersDTO;

    @Pattern(regexp = "^((?!emptyOption).)*$", message = "Wybierz jedną z profesji")
    private String professionName;

    public ClientDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<PhoneNumberDTO> getPhoneNumbersDTO() {
        return phoneNumbersDTO;
    }

    public void setPhoneNumbersDTO(List<PhoneNumberDTO> phoneNumbersDTO) {
        this.phoneNumbersDTO = phoneNumbersDTO;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }
}
