package com.guziec.clientPhoneApp.model.dto;

public class ProfessionDTO {

    private String name;

    private String businessName;

    public ProfessionDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
}
