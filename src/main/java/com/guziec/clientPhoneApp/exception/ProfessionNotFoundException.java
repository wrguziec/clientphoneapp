package com.guziec.clientPhoneApp.exception;

public class ProfessionNotFoundException extends RuntimeException {

    public ProfessionNotFoundException() {
    }

    public ProfessionNotFoundException(String message) {
        super(message);
    }
}
