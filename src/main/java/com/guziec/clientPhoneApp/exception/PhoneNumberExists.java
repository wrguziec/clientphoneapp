package com.guziec.clientPhoneApp.exception;

public class PhoneNumberExists extends RuntimeException {

    public PhoneNumberExists() {
    }

    public PhoneNumberExists(String message) {
        super(message);
    }
}
