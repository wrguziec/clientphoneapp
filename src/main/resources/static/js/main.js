$(document).ready(function () {

    colorClients();

    $(document).on('click', 'button[id^="bth-add"]', function (event) {
        event.preventDefault();
        var buttonId = $(this).attr("id");
        var clientId = buttonId.substring('bth-add'.length, buttonId.length);
        addPhoneNumber(clientId);
    });

    $('#refresh').click(function (event) {
        event.preventDefault();
        refreshClientList();
    });
});

function addPhoneNumber(clientId) {
    var phoneNumber = {
        number: $("#phoneNumber" + clientId).val(),
        clientId: $("#clientId" + clientId).val()
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "clients/addPhoneNumber",
        data: JSON.stringify(phoneNumber),
        cache: false,
        timeout: 600000,
        success: function () {
            appendNewPhoneNumber(clientId, phoneNumber);
            hideErrorMessageForPhoneNumberPatten(clientId);
            hideErrorMessageForNumberExists(clientId);
            clearPhoneNumberInput(clientId);
            colorClients();
        },
        error: function (e) {
            if (e.responseJSON.message.indexOf('NumberExists') !== -1) {
                showErrorMessageForNumberExists(clientId);
                hideErrorMessageForPhoneNumberPatten(clientId);
            } else if (e.responseJSON.status === 400) {
                showErrorMessageForPhoneNumberPatten(clientId);
                hideErrorMessageForNumberExists(clientId);
            }
        }
    });
}

function refreshClientList() {
    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "clients/refresh",
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#clientTable').replaceWith(data);
            colorClients();
        },
        error: function (e) {
        }
    });
}

function colorClients() {
    $('tr[id^="clientRow"]').each(function () {
            var trId = $(this).attr("id");
            var clientId = trId.substring('clientRow'.length, trId.length);
            var phoneNumbersCount = $('#phoneNumbers' + clientId + ' ul li').length;
            var color = chooseColor(phoneNumbersCount);
            $('#' + trId).css('background', color);
        }
    );
}

function chooseColor(clientCount) {
    switch (clientCount) {
        case 0:
            return '#FF4136';
        case 1:
            return '#FFDC00';
        case 2:
            return '#2ECC40';
        default:
            return '#0074D9';
    }
}

function appendNewPhoneNumber(clientId, phoneNumber) {
    var phoneNumbersListId = "#phoneNumbers" + clientId;
    $(phoneNumbersListId + " ul").append('<li class="list-group-item">' + phoneNumber.number + '</li>');
}

function hideErrorMessageForPhoneNumberPatten(clientId) {
    document.getElementById("phoneNumberPatternNotMatched" + clientId).className = 'hidden';
}

function hideErrorMessageForNumberExists(clientId) {
    document.getElementById("numberExistsError" + clientId).className = 'hidden';
}

function showErrorMessageForPhoneNumberPatten(clientId) {
    document.getElementById("phoneNumberPatternNotMatched" + clientId).className = 'error-message';
}

function showErrorMessageForNumberExists(clientId) {
    document.getElementById("numberExistsError" + clientId).className = 'error-message';
}

function clearPhoneNumberInput(clientId) {
    $("#phoneNumber" + clientId).val('');
}
